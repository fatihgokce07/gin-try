package util

import "gitlab.com/fatihgokce07/gin-try/server/core/setting"

// Setup Initialize the util
func Setup() {
	jwtSecret = []byte(setting.AppSetting.JwtSecret)
}

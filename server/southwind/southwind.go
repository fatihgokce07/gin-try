package southwind

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Employee struct {
	gorm.Model
	FirstName string `gorm:"not null;size:30"`
	LastName  string `gorm:"not null;size:30"`
	Emails    []Email
}

type Email struct {
	//	gorm.Model
	EmployeeID int    `gorm:"index"`
	Mail       string `gorm:"type:varchar(50);unique_index"`
	IsActive   bool
}
type Actor struct {
	ActorID    uint      `gorm:"primary_key;column:actor_id"`
	FirstName  string    `gorm:"not null;type:varchar(45)"`
	LastName   string    `gorm:"not null;type:varchar(45)"`
	LastUpdate time.Time `gorm:"not null;type:timestamp"`
}

// func (Actor) TableName() string {
// 	return "actor"
// }

package jwt

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/fatihgokce07/gin-try/server/core/codes"

	"gitlab.com/fatihgokce07/gin-try/server/core/util"
)

// JWT is jwt middleware
func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var data interface{}

		code = codes.SUCCESS
		//token := c.Query("token")
		token2 := c.GetHeader("token")
		fmt.Printf("%v /n", token2)
		if token2 == "" {
			code = codes.INVALID_PARAMS
		} else {
			_, err := util.ParseToken(token2)
			if err != nil {
				switch err.(*jwt.ValidationError).Errors {
				case jwt.ValidationErrorExpired:
					code = codes.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
				default:
					code = codes.ERROR_AUTH_CHECK_TOKEN_FAIL
				}
			}
		}

		if code != codes.SUCCESS {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  codes.GetMsg(code),
				"data": data,
			})

			c.Abort()
			return
		}

		c.Next()
	}
}

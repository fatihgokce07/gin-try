package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/fatihgokce07/gin-try/server/core"
	"gitlab.com/fatihgokce07/gin-try/server/core/codes"
	"gitlab.com/fatihgokce07/gin-try/server/core/util"

	//"github.com/EDDYCJY/go-gin-example/pkg/e"
	//"github.com/EDDYCJY/go-gin-example/pkg/util"
	"gitlab.com/fatihgokce07/gin-try/server/service/auth_service"
)

type auth struct {
	Username string
	Password string
}

func GetAuth(c *gin.Context) {
	appG := core.Gin{C: c}
	//valid := validation.Validation{}

	username := c.Query("username")
	password := c.Query("password")

	//a := auth{Username: username, Password: password}
	// ok, _ := valid.Valid(&a)

	// if !ok {
	// 	app.MarkErrors(valid.Errors)
	// 	appG.Response(http.StatusBadRequest, codes.INVALID_PARAMS, nil)
	// 	return
	// }

	authService := auth_service.Auth{Username: username, Password: password}
	isExist, err := authService.Check()
	if err != nil {
		appG.Response(http.StatusInternalServerError, codes.ERROR_AUTH_CHECK_TOKEN_FAIL, nil)
		return
	}

	if !isExist {
		appG.Response(http.StatusUnauthorized, codes.ERROR_AUTH, nil)
		return
	}

	token, expireTime, err := util.GenerateToken(username, password)
	if err != nil {
		appG.Response(http.StatusInternalServerError, codes.ERROR_AUTH_TOKEN, nil)
		return
	}

	appG.Response(http.StatusOK, codes.SUCCESS, map[string]string{
		"token":      token,
		"expireTime": expireTime.String(),
	})
}

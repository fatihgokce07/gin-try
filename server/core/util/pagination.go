package util

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/fatihgokce07/gin-try/server/core/setting"
	//"github.com/EDDYCJY/go-gin-example/pkg/setting"
)

// GetPage get page parameters
func GetPage(c *gin.Context) int {
	result := 0
	page, _ := strconv.Atoi(c.Query("page")) //com.StrTo(c.Query("page")).MustInt()
	if page > 0 {
		result = (page - 1) * setting.AppSetting.PageSize
	}

	return result
}

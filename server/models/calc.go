package models

func Calculate(x int) int {
	if x > 10 {
		return x * 10
	} else {
		return x / 10
	}
}

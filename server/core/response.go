package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/fatihgokce07/gin-try/server/core/codes"
)

type Gin struct {
	C *gin.Context
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Response setting gin.JSON
func (g *Gin) Response(httpCode, errCode int, data interface{}) {
	g.C.JSON(httpCode, Response{
		Code: errCode,
		Msg:  codes.GetMsg(errCode),
		Data: data,
	})
	return
}

// BindAndValid binds and validates data
func BindAndValid(c *gin.Context, form interface{}) (int, int) {
	err := c.Bind(form)
	if err != nil {
		return http.StatusBadRequest, codes.INVALID_PARAMS
	}

	// valid := validation.Validation{}
	// check, err := valid.Valid(form)
	// if err != nil {
	// 	return http.StatusInternalServerError, e.ERROR
	// }
	// if !check {
	// 	MarkErrors(valid.Errors)
	// 	return http.StatusBadRequest, e.INVALID_PARAMS
	// }

	return http.StatusOK, codes.SUCCESS
}

// MarkErrors logs error logs
// func MarkErrors(errors []*validation.Error) {
// 	for _, err := range errors {
// 		logging.Info(err.Key, err.Message)
// 	}

// 	return
// }

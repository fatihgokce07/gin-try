module gitlab.com/fatihgokce07/gin-try/server

require (
	cloud.google.com/go v0.44.3 // indirect
	github.com/360EntSecGroup-Skylar/excelize v1.4.1 // indirect
	github.com/EDDYCJY/go-gin-example v0.0.0-20190818085528-dea644391d2b
	github.com/appleboy/gin-jwt/v2 v2.6.2
	github.com/astaxie/beego v1.12.0
	github.com/bradfitz/gomemcache v0.0.0-20190329173943-551aad21a668 // indirect
	github.com/casbin/casbin v1.9.1 // indirect
	github.com/couchbase/go-couchbase v0.0.0-20190828225015-86aa785ef73d // indirect
	github.com/couchbase/gomemcached v0.0.0-20190608153736-7c1f4fd19ef7 // indirect
	github.com/couchbase/goutils v0.0.0-20190315194238-f9d42b11473b // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190820223206-44cdfe8d8ba9 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/contrib v0.0.0-20190526021735-7fb7810ed2a0
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.46.0
	github.com/go-redis/redis v6.15.2+incompatible // indirect
	github.com/gogo/protobuf v1.2.2-0.20190730201129-28a6bbf47e48 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190812055157-5d271430af9f // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/pat v1.0.1 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rogpeppe/go-internal v1.3.1 // indirect
	github.com/siddontang/ledisdb v0.0.0-20190202134119-8ceb77e66a92 // indirect
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tealeg/xlsx v1.0.3 // indirect
	github.com/tidwall/gjson v1.3.2 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	github.com/unknwon/com v1.0.1
	github.com/wendal/errors v0.0.0-20181209125328-7f31f4b264ec // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
	golang.org/x/exp v0.0.0-20190829153037-c13cbed26979 // indirect
	golang.org/x/image v0.0.0-20190829233526-b3c06291d021 // indirect
	golang.org/x/mobile v0.0.0-20190826170111-cafc553e1ac5 // indirect
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
	golang.org/x/sys v0.0.0-20190830142957-1e83adbbebd0 // indirect
	golang.org/x/tools v0.0.0-20190830154057-c17b040389b9 // indirect
	google.golang.org/api v0.9.0 // indirect
	google.golang.org/appengine v1.6.2 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
	google.golang.org/grpc v1.23.0 // indirect
	gopkg.in/ini.v1 v1.46.0 // indirect
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)

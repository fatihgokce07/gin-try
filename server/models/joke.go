package models

func Add() int {
	return 3 + 3
}
func Sub(x, y int) int {
	return x - y
}

type Joke struct {
	ID    int    `json:"id" binding:"required"`
	Likes int    `json:"likes"`
	Joke  string `json:"joke" binding:"required"`
}

func (r *Joke) GetId() int {
	return r.ID * 2
}

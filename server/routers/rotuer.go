package routers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/fatihgokce07/gin-try/server/api"
	"gitlab.com/fatihgokce07/gin-try/server/middleware/jwt"
	// "github.com/EDDYCJY/go-gin-example/middleware/jwt"
	// "github.com/EDDYCJY/go-gin-example/pkg/export"
	// "github.com/EDDYCJY/go-gin-example/pkg/qrcode"
	// "github.com/EDDYCJY/go-gin-example/pkg/upload"
	// "github.com/EDDYCJY/go-gin-example/routers/api"
	// "github.com/EDDYCJY/go-gin-example/routers/api/v1"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	// r.StaticFS("/export", http.Dir(export.GetExcelFullPath()))
	// r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))
	// r.StaticFS("/qrcode", http.Dir(qrcode.GetQrCodeFullPath()))

	r.GET("/auth", api.GetAuth)
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	// r.POST("/upload", api.UploadImage)

	apiv1 := r.Group("/api/v1")
	apiv1.Use(jwt.JWT())
	{

		apiv1.GET("/tags", api.GetTags)

		apiv1.POST("/tags", api.AddTag)

		apiv1.PUT("/tags/:id", api.EditTag)

		apiv1.DELETE("/tags/:id", api.DeleteTag)

	}

	return r
}
